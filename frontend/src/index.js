import React from 'react';
import ReactDOM from 'react-dom/client';
import Main from './Components/Main';

import "../styles/styles.scss";

ReactDOM.createRoot(document.getElementById("root")).render(<Main />);