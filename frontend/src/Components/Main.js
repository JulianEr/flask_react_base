import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Main = () => {
    const [displayed, setDisplayed] = useState("Loading...");

    useEffect(() => {
        const controller = new AbortController();
        axios
            .get(`api/hello`, { signal: controller.signal })
            .then((result) => {
                const data = result.data
                if(data){
                    setDisplayed(data);
                }
            })
            .catch((err) => {
                if (axios.isCancel(err)) {
                    console.log("Cancelled!");
                }
            });

        return () => {
            controller.abort();
        }
    }, []);

    return (<p>{displayed}</p>)
}

export default Main;