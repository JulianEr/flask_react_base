from flask import Blueprint

api = Blueprint("api", __name__, url_prefix='/api')

@api.route("/hello", methods=['GET'])
def api_list():
    return "Hello World"