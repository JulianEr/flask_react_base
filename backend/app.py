from flask import Flask, redirect, render_template, url_for
from api import api

app = Flask(__name__)
app.register_blueprint(api.api)

@app.route("/")
def root():
    return redirect(url_for('ui'))

@app.route("/ui")
def ui():
    return render_template('index.html')

